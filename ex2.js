import data from './people.json' assert { type: 'json'}

var peopleResult = []

// find data
function filterBy(data, key, value) {
  peopleResult = data.filter(
    p => p.name.toLowerCase().includes(value)
      || p.address.toLowerCase().includes(value)
      || p.category.toLowerCase().includes(value)
  ).sort((p1, p2) => p1.name < p2.name ? -1 : p1.name > p2.name ? 1 : 0); // sort data a-z
  return peopleResult
}
// sort data
/// sắp xếp dữ liệu theo key truyền vào
function sortBy(data) {
  peopleResult = data.sort((p1, p2) => p1.name < p2.name ? -1 : p1.name > p2.name ? 1 : 0); // sort data a-z
  return peopleResult
}

/// gom nhóm địa chỉ
function groupByAddress(data) {
  return data.reduce(function (acc, cur) {
    if (!acc.some(function (x) { return x[0].address == cur.address })) {
      let group = data.filter(function (x) { return x.address == cur.address; });
      acc.push(group)
    }
  }, [])
}

/// tìm kiếm địa chỉ gần giống nhau hoặc giống nhau
function findSimilarAddress(data, address) {
  return data.filter(function (x) {
    return x.address.includes(address);
  });
}

// render content
function renderContent(content = data) {

  let searchResult = document.getElementById('search-result')
  searchResult.innerHTML = '';
  content.forEach(p => {
    let person = document.createElement('div');
    person.className = 'person';

    let image = document.createElement('img');
    image.src = '/asset/image/people.png';

    let right = document.createElement('div');
    right.className = 'right';
    let name = document.createElement('div');
    name.className = 'name';
    name.innerText = p.name;
    right.appendChild(name);
    let address = document.createElement('div');
    address.className = 'address';
    address.innerText = p.address;
    right.appendChild(address);
    let category = document.createElement('div');
    category.className = 'category';
    category.innerText = p.category;
    right.appendChild(category);

    person.appendChild(image);
    person.appendChild(right);
    searchResult.appendChild(person);
  })
}
renderContent()


// handle event typing input, click button
function handleInputText(event) {
  let content = filterBy(data, 'name', event.target.value)
  renderContent(content)
}

function handleInputAZ() {
  let list = peopleResult.length > 0 ? peopleResult : data

  let content = sortBy(list)
  renderContent(content)
}

function handleInputAddress() { }

let inputText = document.getElementById('inputText')
inputText.addEventListener('input', handleInputText)

let inputaz = document.getElementById('btnAZ')
inputaz.addEventListener('click', handleInputAZ)

let inputaddress = document.getElementById('btnAddress')
inputaddress.addEventListener('click', handleInputAddress)


// handle btn clear search
function clearSearch() {
  inputText.value = ''
  renderContent()
}

let btnClear = document.getElementById('btnClear')
btnClear.addEventListener('click', clearSearch)