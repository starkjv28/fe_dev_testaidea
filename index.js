let carousel = document.querySelector('.carousel')
let isDragStart = false, prevPageX, prevScrollLeft


// handle event drag by mouse
const dragging = (e) => {
  if (!isDragStart) {
    return
  }
  e.preventDefault()

  let positionDiff = e.pageX - prevPageX
  carousel.scrollLeft = prevScrollLeft - positionDiff
}

const dragStart = (e) => {
  isDragStart = true
  prevPageX = e.pageX
  prevScrollLeft = carousel.scrollLeft
}

const dragStop = (e) => {
  isDragStart = false
}

// handle event wheel by mouse
const onwheel = (e) => {
  prevScrollLeft = carousel.scrollLeft
  if (e.wheelDelta > 0) {
    carousel.scrollLeft -= 206
  } else {
    carousel.scrollLeft += 206
  }
}

// add event dragg by mouse
carousel.addEventListener("mousemove", dragging)
carousel.addEventListener("mousedown", dragStart)
carousel.addEventListener("mouseup", dragStop)

// add event wheel by mouse
carousel.addEventListener('wheel', onwheel)


//render dot dot dot.....
const renderDot = () => {
  let listItem = document.getElementsByClassName('item')
  let dotCount = listItem.length / 3
  var dots = []

  let dotContainer = document.querySelector('.dot-container')
  for (let i = 0; i <= dotCount - 1; i++) {
    let dot = document.createElement('div')
    dot.className = 'dot'
    dot.onclick = (e) => {
      carousel.scrollLeft = i * 208 * 3
      dots.forEach(d => d.className = 'dot')
      e.target.className = 'dot selected'
    }
    dotContainer.appendChild(dot)
    dots.push(dot)
  }
}
renderDot()